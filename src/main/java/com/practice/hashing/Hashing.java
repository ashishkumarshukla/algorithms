package com.practice.hashing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Hashing {

	public int countOfDistinctElementsInIntersectionOfTwoArrays(int[] arrOne,
			int[] arrTwo) {
		Set<Integer> set = new HashSet<>();
		int res = 0;
		for (int i : arrOne)
			set.add(i);

		for (int j : arrTwo) {
			if (set.contains(j)) {
				res++;
				set.remove(j);
			}
		}
		return res;
	}

	public int countOfDistinctElementsInUnionOfTwoArrays(int[] arrOne,
			int[] arrTwo) {
		Set<Integer> set = new HashSet<>();
		for (int i : arrOne)
			set.add(i);
		for (int j : arrTwo) {
			set.add(j);
		}
		return set.size();
	}

	public boolean isPairWithGivenSum(int[] arrOne, int sum) {
		Set<Integer> set = new HashSet<>();
		for (int i : arrOne) {
			set.add(i);
			if (set.contains(sum - i))
				return true;
		}
		return false;
	}
	
	public boolean isSubarrayWithZeroSum(int[] arrOne) {
		Set<Integer> set = new HashSet<>();
		int prefixSum=0;
		for (int i : arrOne) {
			prefixSum+=i;
			if(set.contains(prefixSum) ||prefixSum==0)
				return true;
			set.add(prefixSum);
		}
		
		return false;
	}
	
	public boolean isSubarrayWithGivenSum(int[] arrOne,int sum) {
		Set<Integer> set = new HashSet<>();
		int prefixSum=0;
		for (int i : arrOne) {
			prefixSum+=i;
			if(set.contains(prefixSum-sum)||prefixSum==sum)
				return true;
			set.add(prefixSum);
		}
		
		return false;
	}
	
	public int longestSubarrayWithGivenSum(int[] arrOne,int sum) {
		int prefixSum=0;
		int res=0;
		Map<Integer,Integer> map=new HashMap<>();
		
		for(int i=0;i<arrOne.length;i++) {
			prefixSum+=arrOne[i];
			if(prefixSum==sum)
				res=i+1;
			if(!map.containsKey(prefixSum))
				map.put(prefixSum, i);
			if(map.containsKey(prefixSum-sum))
				if(res<(i-map.get(prefixSum-sum)))
					res=i-map.get(prefixSum-sum);
		}
		return res;
	}
	
	public int longestSubarrayWithEqualZeroAndOne(int[] arrOne) {
		int res = 0;
		Map<Integer, Integer> map = new HashMap<>();
		map.put(1, 0);
		map.put(0, 0);
		for (int i = 0; i < arrOne.length; i++) {
			map.put(arrOne[i], map.get(arrOne[i]) + 1);
			if (map.get(0) == map.get(1))
				res = map.get(0)*2;

		}
		return res;
	}
	
	public int longestCommonSpanWithSameSumInBinaryArray(int[] arrOne,int[] arrTwo) {
		int[] temp=new int[arrOne.length];
		for(int i=0;i<arrOne.length;i++)
			temp[i]=arrOne[i]-arrTwo[i];
		
		return longestSubarrayWithGivenSum(temp,0);
			
	}
	
}
