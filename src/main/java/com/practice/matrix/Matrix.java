package com.practice.matrix;

public class Matrix {

	public void snakePattern(int[][] matrix) {
		int rowLen;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				rowLen = matrix[i].length;
				if (i % 2 == 0)
					System.out.print(matrix[i][j] + " ");
				else
					System.out.print(matrix[i][rowLen - 1 - j] + " ");
			}
			System.out.println();
		}

	}

	public int[][] transpose(int[][] matrix) {
		int rowLen;
		for (int i = 0; i < matrix.length; i++) {
			rowLen = matrix[i].length;
			for (int j = i + 1; j < rowLen; j++) {
				swap(matrix, i, j);
			}
		}

		return matrix;
	}

	private void swap(int[][] matrix, int i, int j) {
		int temp;
		temp = matrix[i][j];
		matrix[i][j] = matrix[j][i];
		matrix[j][i] = temp;
	}

	public int[][] rotateByNinetyDegreeCounterClock(int[][] matrix) {
		int rowLen;
		int temp;
		for (int i = 0; i < matrix.length; i++) {
			rowLen = matrix[i].length;
			for (int j = i + 1; j < rowLen; j++) {
				swap(matrix, i, j);
			}
		}

		for (int i = 0; i < matrix.length / 2; i++) {
			rowLen = matrix[i].length;
			for (int j = 0; j < rowLen; j++) {
				temp = matrix[i][j];
				matrix[i][j] = matrix[rowLen - 1 - i][j];
				matrix[rowLen - 1 - i][j] = temp;
			}

		}

		return matrix;
	}

	public void rotateByNinetyDegreeClockWise(int[][] matrix) {
		int rowLen;
		int temp;
		for (int i = 0; i < matrix.length / 2; i++) {
			rowLen = matrix[i].length;
			for (int j = 0; j < rowLen; j++) {
				temp = matrix[i][j];
				matrix[i][j] = matrix[rowLen - 1 - i][j];
				matrix[rowLen - 1 - i][j] = temp;
			}
		}
		for (int i = 0; i < matrix.length; i++) {
			rowLen = matrix[i].length;
			for (int j = i + 1; j < rowLen; j++) {
				swap(matrix, i, j);
			}
		}
	}
	
	public void boundaryTraversal(int[][] matrix) {
		boolean isSingleColumn = true;
		
		for (int j = 0; j < matrix.length; j++) {
			if (matrix[j].length != 1) {
				isSingleColumn = false;
				break;
			}
		}
		
		if (matrix.length == 1) {
			for (int j = 0; j < matrix[0].length; j++)
				System.out.print(matrix[0][j] + " ");

		}

		else if (isSingleColumn) {
			for (int j = 0; j < matrix.length; j++)
				System.out.print(matrix[j][0] + " ");

		} else {

			for (int j = 0; j < matrix[0].length; j++)
				System.out.print(matrix[0][j] + " ");

			for (int i = 1; i < matrix.length; i++)
				System.out.print(matrix[i][matrix[i].length - 1] + " ");

			for (int k = matrix[matrix.length - 1].length - 2; k >= 0; k--)
				System.out.print(matrix[matrix.length - 1][k] + " ");

			for (int k = matrix.length - 2; k > 0; k--)
				System.out.print(matrix[k][0] + " ");
		}
	}
	public void spiralTraversal(int[][] matrix) {
		int top = 0;
		int left = 0;
		int bottom = matrix.length - 1;
		int right = matrix[0].length - 1;

		while (top <= bottom && left <= right) {
			for (int i = left; i <= right; i++)
				System.out.print(matrix[top][i] + " ");
			top++;

			for (int j = top; j <= bottom; j++)
				System.out.print(matrix[j][right] + " ");
			right--;

			if (top <= bottom) {
				for (int i = right; i >= left; i--)
					System.out.print(matrix[bottom][i] + " ");
				bottom--;
			}
			if (left <= right) {
				for (int i = bottom; i >= top; i--)
					System.out.print(matrix[i][left] + " ");
				left++;
			}
		}
	}
	
	public boolean rowWiseAndColumnWiseSortedKeySearch(int[][] matrix,int key) {
		int top = 0;
		int right = matrix.length - 1;

		while (top <= matrix.length - 1 && right >= 0) {
			if (key == matrix[top][right])
				return true;
			if (key > matrix[top][right])
				top++;
			else
				right--;
		}

		return false;

	}
}