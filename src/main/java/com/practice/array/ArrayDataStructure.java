package com.practice.array;

public class ArrayDataStructure {

	public void leftRotateByOne(int arr[]) {
		int temp = arr[0];
		int len = arr.length;
		for (int i = 0; i < len - 1; i++)
			arr[i] = arr[i + 1];
		arr[len - 1] = temp;

	}

	public void leftRotateByD(int arr[], int d) {
		int len = arr.length - 1;
		reverse(arr, 0, d - 1);
		reverse(arr, d, len);
		reverse(arr, 0, len);
	}

	private void reverse(int[] arr, int low, int high) {

		while (low < high) {
			swap(arr, low, high);
			low++;
			high--;
		}

	}

	private void swap(int[] arr, int low, int high) {
		int temp = arr[low];
		arr[low] = arr[high];
		arr[high] = temp;
	}

	public void leadersInArray(int[] arr) {
		int len = arr.length;
		int currentLeader = arr[len - 1];
		System.out.println(currentLeader);
		for (int i = len - 2; i >= 0; i--) {
			if (arr[i] > currentLeader) {
				currentLeader = arr[i];
				System.out.println(currentLeader);
			}
		}
	}

	public int maxDifference(int[] arr) {
		int min = arr[0];
		int res = 0;
		int diff;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i];
			}
			diff = arr[i] - min;
			if (res < diff) {
				res = diff;
			}
		}
		return res;
	}

	public int maxProfitFromBuySaleShare(int[] arr) {
		int profit = 0;
		for (int i = 0; i < arr.length - 1; i++) {
			if (arr[i] < arr[i + 1])
				profit += arr[i + 1] - arr[i];
		}
		return profit;
	}

	public int trappingRainWater(int[] arr) {

		int totalWater = 0;
		for (int i = 1; i < arr.length - 1; i++) {
			int leftMax = arr[i];

			for (int j = 0; j < i; j++) {
				if (leftMax < arr[j])
					leftMax = arr[j];
			}

			int rightMax = arr[i];

			for (int j = i + 1; j < arr.length; j++) {
				if (rightMax < arr[j])
					rightMax = arr[j];
			}

			totalWater += Math.min(leftMax, rightMax) - arr[i];

		}
		return totalWater;
	}

	public int trappingRainWaterOptimized(int[] arr) {
		int totalWater = 0;
		int[] leftMax = new int[arr.length];
		int[] rightMax = new int[arr.length];

		leftMax[0] = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (leftMax[i - 1] < arr[i])
				leftMax[i] = arr[i];
			else
				leftMax[i] = leftMax[i - 1];
		}
		rightMax[arr.length - 1] = arr[arr.length - 1];
		for (int i = arr.length - 2; i >= 0; i--) {
			if (rightMax[i + 1] < arr[i])
				rightMax[i] = arr[i + 1];
			else
				rightMax[i] = rightMax[i + 1];
		}

		for (int i = 1; i < arr.length - 1; i++)
			totalWater += Math.min(leftMax[i], rightMax[i]) - arr[i];

		return totalWater;
	}

	public int maxSubArray(int arr[]) {
		int sum = arr[0];
		int currMax = arr[0];
		int start = 0;
		int end = 0;
		int startIndex = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] + currMax > arr[i]) {
				currMax = arr[i] + currMax;

			} else {
				currMax = arr[i];
				startIndex += 1;
			}

			if (currMax > sum) {
				sum = currMax;
				end = i;
				start = startIndex;
			}
		}
		System.out.println("Start index " + start + " End index " + end);
		return sum;
	}

	public int maxEvenOddSubArray(int[] arr) {
		int count = 0;
		int currLen = 0;
		for (int i = 0; i < arr.length - 1; i++) {
			if ((arr[i] % 2 == 0 && arr[i + 1] % 2 != 0) || (arr[i] % 2 != 0 && arr[i + 1] % 2 == 0)) {
				count++;
			} else {
				count = 0;
			}

			if (currLen < count)
				currLen = count;
		}
		return currLen + 1;
	}

	public int mooreMajorityElement(int[] arr) {
		int res = 0;
		int count = 1;
		for (int i = 1; i < arr.length; i++) {
			if (arr[res] == arr[i])
				count++;
			else
				count--;
			if (count == 0) {
				res = i;
				count = 1;
			}

		}
		count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == arr[res])
				count++;
		}
		if (count <= arr.length / 2)
			return -1;

		return arr[res];

	}

	public int windowSliding(int[] arr, int k) {
		int maxSum = 0;
		int currSum;
		for (int i = 0; i <= arr.length - k; i++) {
			currSum = 0;
			for (int j = i; j < i + 3; j++) {
				currSum += arr[j];
			}
			if (maxSum < currSum)
				maxSum = currSum;
		}
		return maxSum;
	}

	public int windowSlidingOptimized(int[] arr, int k) {
		int currSum = 0;
		int maxSum;
		for (int i = 0; i < k; i++)
			currSum += arr[i];
		maxSum = currSum;
		for (int i = k; i < arr.length; i++) {
			currSum += arr[i] - arr[i - k];
			if (currSum > maxSum)
				maxSum = currSum;
		}
		return maxSum;
	}

	public boolean subSumNonNegativeElements(int[] arr, int sum) {
		int currSum = arr[0];
		int start = 0;
		for (int end = 1; end < arr.length; end++) {
			while (currSum > sum && start < end - 1) {
				currSum -= arr[start];
				start++;
			}
			if (currSum == sum)
				return true;
			if (end < arr.length) {
				currSum += arr[end];
			}
		}
		return currSum - arr[start] == sum;

	}

	public int[] mNbonacci(int m, int n) {
		int currSum = 1;
		int[] arr = new int[n];
		for (int i = 0; i < m - 1; i++)
			arr[i] = 0;
		arr[m - 1] = 1;
		for (int i = m; i < n; i++) {
			arr[i] = currSum;
			currSum += arr[i] - arr[i - m];

		}
		return arr;

	}
}