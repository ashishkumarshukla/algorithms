package com.practice.queue;

import java.util.LinkedList;
import java.util.Queue;
public class StackUsingQueue<T> {

	private Queue<T> queueOne, queueTwo;

	public StackUsingQueue() {
		queueOne = new LinkedList<T>();
		queueTwo = new LinkedList<T>();
	}

	public int size() {
		return queueOne.size();
	}

	public T pop() {
		return queueOne.remove();
	}

	public void push(T element) {
		while (queueOne.isEmpty() == false) {
			queueTwo.add(queueOne.remove());
		}

		queueOne.add(element);

		while (queueTwo.isEmpty() == false) {
			queueOne.add(queueTwo.remove());
		}

	}

	public void add(T element) {
		queueOne.add(element);
	}
	
	public void remove() {
		while (queueOne.size() != 1) {
			queueTwo.add(queueOne.remove());
		}
		queueOne.remove();
		Queue<T> q = queueOne;
		queueOne = queueTwo;
		queueTwo = q;
	}
	
}
