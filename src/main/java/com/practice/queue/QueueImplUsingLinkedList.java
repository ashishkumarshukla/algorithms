package com.practice.queue;

public class QueueImplUsingLinkedList<T> {
	Node front;
	Node rear;
	private class Node {
		T data;
		Node next;
		Node(T d) {
			data = d;
		}
	}

	public void enqueue(T t) {
		Node temp = new Node(t);
		if (front == null) {
			front = rear=temp;
			return;
		}
		rear.next=temp;
		rear=temp;
		
	}

	public void dequeue() {
		if (front == null)
			throw new RuntimeException("Queue is empty");
		front=front.next;
		if (front == null)
			rear=null;
		
		

	}

}
 