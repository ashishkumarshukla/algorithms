package com.practice.queue;

import java.util.LinkedList;
import java.util.Queue;

public class ProblemSolving{

	public <T> void reverseQueue(Queue<T> queue) {
		
		if(queue.isEmpty())
			return ;

		T temp = queue.remove();
		reverseQueue(queue);
		queue.add(temp);
		
	}
	
	public static void printFirstNNumbers(int n, int [] digits) {
		
		Queue<String> queue=new LinkedList<>();
		for(int i:digits)
			queue.add(i+"");
		for(int i=0;i<n;i++) {
			String curr=queue.remove();
			System.out.println(curr+" ");
			if(curr.equalsIgnoreCase("0"))continue;
			for(int d:digits)
				queue.add(curr+d);
		}
		
	}
	
}
