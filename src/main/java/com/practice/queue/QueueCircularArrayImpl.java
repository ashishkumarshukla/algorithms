package com.practice.queue;

public class QueueCircularArrayImpl<T> {

	private int cap;
	private T[] arr;
	private int size;
	private int front;

	public QueueCircularArrayImpl() {
		size = 0;
		front = 0;
		cap = 10;
		arr = (T[]) new Object[cap];
	}

	public QueueCircularArrayImpl(int cap) {
		size = 0;
		front = 0;
		this.cap = cap;
		arr = (T[]) new Object[cap];
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == cap;
	}

	public void enqueue(T t) {
		if (isFull())
			throw new RuntimeException("Queue is Full");
		else {
			int rear = getRear();
			rear = (rear + 1) % cap;
			arr[rear] = t;
			size++;
		}

	}
	public void dequeue() {
		if (isEmpty())
			throw new RuntimeException("Queue is Empty");
		else {
			front = (front + 1) % cap;
			size--;
		}

	}

	private int getFront() {
		if (isEmpty())
			return -1;
		return front;
	}

	private int getRear() {
		if (isEmpty())
			return -1;
		return (front + size - 1) % cap;
	}
	
	public T getFrontElement() {
		if(isEmpty())
			throw new RuntimeException("Queue is Empty");
		return arr[front];
	}
	
	public T getRearElement() {
		if(isEmpty())
			throw new RuntimeException("Queue is Empty");
		return arr[getRear()];
	}

}
