package com.practice.queue;

public class Queue<T> {

	private int cap;
	private T[] arr;
	private int size;

	public Queue() {
		size = 0;
		cap = 10;
		arr = (T[]) new Object[cap];
	}

	public Queue(int cap) {
		size = 0;
		this.cap = cap;
		arr = (T[]) new Object[cap];
	}

	public void enqueue(T t) {
		if (isFull())
			throw new RuntimeException("Queue is Full");
		else
			arr[size++] = t;
	}
	public void dequeue() {
		if (isEmpty())
			throw new RuntimeException("Queue is Empty");
		else {
			for (int i = 0; i < size - 1; size++)
				arr[i] = arr[i + 1];
			size--;
		}

	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == cap;
	}
	
	public T getFrontElement() {
		if(isEmpty())
			throw new RuntimeException("Queue is Empty");
		return arr[0];
	}
	
	public T getRearElement() {
		if(isEmpty())
			throw new RuntimeException("Queue is Empty");
		return arr[size-1];
	}

}
