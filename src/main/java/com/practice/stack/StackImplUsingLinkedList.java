package com.practice.stack;

import java.util.EmptyStackException;

public class StackImplUsingLinkedList<T> {
	Node head;
	class Node {
		T data;
		Node next;
		Node(T d) {
			data = d;
		}
	}

	public T push(T x) {
		Node temp = new Node(x);
		temp.next = head;
		head = temp;
		return x;
	}

	public T pop() {
		if (head == null)
			throw new EmptyStackException();
		Node temp = head;
		head = head.next;
		return temp.data;

	}

	public T peek() {
		if (head == null)
			throw new EmptyStackException();
		return head.data;

	}

}
