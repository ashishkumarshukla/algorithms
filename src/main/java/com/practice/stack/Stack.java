package com.practice.stack;

import java.util.EmptyStackException;

public class Stack<T> {
	int top = -1;
	int cap;
	T[] arr;

	Stack() {
		this.cap = 10;
		this.arr = (T[]) new Object[cap];
	}
	
	Stack(int size) {
		this.cap = size;
		this.arr = (T[]) new Object[cap];
	}

	public T push(T element) {
		if (top == cap - 1)
			throw new RuntimeException("Stack is full");
		return arr[++top] = element;

	}

	public T pop() {
		if (top == -1)
			throw new EmptyStackException();
		return arr[top--];

	}

	public T peek() {
		if (top == -1)
			throw new EmptyStackException();
		return arr[top];

	}
	
   public boolean isEmpty() {
	   return top == -1;
   }
   
	
	public boolean isBalanced(String str) {
		
		Stack<Character> s = new Stack<>();
		
		for (int i = 0; i < str.length(); i++) {

			if ('(' == str.charAt(i) || '{' == str.charAt(i) || '[' == str.charAt(i)) 
				
				s.push(str.charAt(i));

			else if (s.isEmpty() || !matching(s.pop(), str.charAt(i)))
				return false;

		}
		return s.isEmpty() == true;
	}

	private boolean matching(char a, char b) {
		return ((a=='(' && b==')') || (a=='{' && b=='}') || (a=='[' && b==']'));
	}
	
}