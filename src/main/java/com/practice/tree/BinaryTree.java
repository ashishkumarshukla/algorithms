package com.practice.tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree<T  extends Comparable<T>>{
	Node root;
	class Node {
		T key;
		Node left;
		Node right;
		Node(T d) {
			key = d;
		}
	}

	public void inOrder(Node root) {
		if (root != null) {
			inOrder(root.left);
			System.out.print(root.key + " ");
			inOrder(root.right);
		}

	}

	public void preOrder(Node root) {
		if (root != null) {
			System.out.print(root.key + " ");
			preOrder(root.left);
			preOrder(root.right);
		}

	}

	public void postOrder(Node root) {
		if(root !=null) {
			postOrder(root.left);
			postOrder(root.right);
			System.out.print(root.key+" ");
		}
		
	}
	
	public void levelOrderTraversal(Node root) {
		if(root==null)
			return;
		Queue<Node> queue=new LinkedList<>();
        queue.add(root);
		while(!queue.isEmpty()) {
			Node curr=queue.poll();
			System.out.print(curr.key+" ");
			if(curr.left!=null)
				queue.add(curr.left);
			if(curr.right!=null)
				queue.add(curr.right);
		}
		
		
	}
	
	public void levelOrderTraversalLineByLine(Node root) {
		if(root==null)
			return;
		Queue<Node> queue=new LinkedList<>();
        queue.add(root);
        queue.add(null);
		while(queue.size()>1) {
			Node curr=queue.poll();
			if(curr==null) {
				System.out.println();
				queue.add(null);
				continue;
			}
			
			System.out.print(curr.key+" ");
			if(curr.left!=null)
				queue.add(curr.left);
			if(curr.right!=null)
				queue.add(curr.right);
		}
		
	}
	
	public void levelOrderTraversalLineByLineWithInnerLoop(Node root) {
		if (root == null)
			return;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int count = queue.size();
			for (int i = 0; i < count; i++) {
				Node curr = queue.poll();
				System.out.print(curr.key + " ");
				if (curr.left != null)
					queue.add(curr.left);
				if (curr.right != null)
					queue.add(curr.right);
			}
			System.out.println();
		}
	}
	
	public int sizeOfBinearyTree(Node root) {
		if(root==null)
			return 0;
		else
		return 1+sizeOfBinearyTree(root.left)+sizeOfBinearyTree(root.right);
	}
	
	public T maxElement(Node root) {
		if(root==null)
			return null;
		else 
			return max(root.key,max(maxElement(root.left),maxElement(root.right)));
	}
	
	public T max(T t1, T t2) {
		if (t1 == null)
			return t2;
		if (t2 == null)
			return t1;
		T max = t1;
		if (t2.compareTo(max) > 0)
			max = t2;
		return max;
	}
	
	public int height(Node root) {
		if(root==null)
			return 0;
		return 1+ Math.max(height(root.left),height(root.right));
	}
	
	public void printNodesAtkDistanceFromRoot(Node root, int k) {
		if (root == null)
			return;
		if (k == 0)
			System.out.print(root.key + " ");
		else {
			printNodesAtkDistanceFromRoot(root.left, k - 1);
			printNodesAtkDistanceFromRoot(root.right, k - 1);

		}
	}
	
	int maxLevel=0;
	public void printLeftViewOfBinaryTree(Node root, int level) {
		if (root == null)
			return;
		if(level<maxLevel) {
			System.out.print(root.key+" ");
			maxLevel=level;
		}
		printLeftViewOfBinaryTree(root.left,level+1);
		printLeftViewOfBinaryTree(root.right,level+1);

	}
	
	public void printLeftViewOfBinaryTreeIterativeMethod(Node root) {
		if (root == null)
			return;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int count = queue.size();
			for (int i = 0; i < count; i++) {
				Node curr = queue.poll();
				if (i == 0)
					System.out.print(curr.key + " ");
				if (curr.left != null)
					queue.add(root.left);
				if (curr.right != null)
					queue.add(root.right);

			}
			System.out.println();
		}
	}
	
	public boolean isCSumProperty() {
		if(this.root==null)
			return true;
		if (!(this.root.key instanceof Integer || this.root.key instanceof Long||this.root.key instanceof Float||this.root.key instanceof Double))
			throw new UnsupportedOperationException("This operation is supported for int, float, double and long key types only");
		
		return isChildrenSumProperty(this.root);
	}
	
	private boolean isChildrenSumProperty(Node root) {
		if (root == null)
			return true;
		if (root.left == null && root.right == null)
			return true;
		double sum = 0;
		if (root.left != null)
			sum += (Double) root.left.key;
		if (root.right != null)
			sum += (Double) root.right.key;
		return ((Double) root.key == sum && isChildrenSumProperty(root.left)
				&& isChildrenSumProperty(root.right));
	}
	
	public int isBalancedTree(Node root) {
		if (root == null)
			return 0;
		int lth = isBalancedTree(root.left);
		if (lth == -1)
			return -1;
		int rth = isBalancedTree(root.right);
		if (rth == -1)
			return -1;
		if (Math.abs(rth - lth) > 1)
			return -1;
		else
			return Math.max(lth, rth) + 1;
	}
	
	public int maxWidthOftree(Node root) {
		if (root == null)
			return 0;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		int res=0;
		while (!queue.isEmpty()) {
			int count = queue.size();
			res=Math.max(res,count);
			for (int i = 0; i < count; i++) {
				Node curr = queue.poll();
				if (curr.left != null)
					queue.add(curr.left);
				if (curr.right != null)
					queue.add(curr.right);
			}
		}
		return res;
	}
	
	Node prev = null;
	public Node binareeToDDL(Node root) {
		if (root == null)
			return root;
		Node head = binareeToDDL(root.left);
		if (prev == null)
			head = root;
		else {
			root.left = prev;
			prev.right = root;
		}
		prev = root;
		binareeToDDL(root.right);
		return head;
	}
	
	int preIndex=0;
	public Node counstructTree(T [] in, T [] pre, int is, int ie) {
		if(is>ie) return null;
		Node root=new Node(pre[preIndex++]);
		
		int inIndex=0;
		for(int i=is;i<=ie;i++) {
			if(in[i]==root.key) {
				inIndex=i;
				break;
				}
		}
		
		root.left=counstructTree(in,pre,is,inIndex-1);
		root.right=counstructTree(in,pre,inIndex+1,ie);
		return root;
	}
}
