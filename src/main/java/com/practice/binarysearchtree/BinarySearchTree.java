package com.practice.binarysearchtree;

import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

public class BinarySearchTree {

	Node root;
	class Node {
		int key;
		Node left;
		Node right;
		Node(int d) {
			key = d;
		}
	}

	public boolean search(Node root, int key) {
		if (root == null)
			return false;
		else if (key == root.key)
			return true;
		else if (key < root.key)
			return search(root.left, key);
		else
			return search(root.right, key);
	}

	public Node insert(Node root, int key) {
		if (root == null)
			return new Node(key);
		else if (key < root.key)
			root.left = insert(root.left, key);
		else if (key > root.key)
			root.right = insert(root.right, key);
		return root;
	}

	public Node insertIterative(Node root, int key) {
		Node temp = new Node(key);
		Node parent = null;
		Node curr = root;
		while (curr != null) {
			parent = curr;
			if (key > root.key)
				curr = root.right;
			else if (key < root.key)
				curr = root.left;
			else
				return root;

		}
		if (parent == null)
			return temp;
		if (key < parent.key)
			parent.left = temp;
		else
			parent.right = temp;
		return root;
	}

	public Node delete(Node root, int key) {
		if (root == null)
			return null;
		else if (key < root.key)
			root.left = delete(root.left, key);
		else if (key > root.key)
			root.right = delete(root.right, key);
		else {
			if (root.left == null)
				return root.right;
			else if (root.right == null)
				return root.left;
			else {
				Node succ = getSuccessorNode(root);
				root.key = succ.key;
				root.right = delete(root.right, succ.key);
			}
		}
		return root;

	}

	private Node getSuccessorNode(Node root2) {
		Node curr = root.right;
		while (curr != null && curr.left != null)
			curr = curr.left;
		return curr;
	}
	
	public Node floor(Node root, int key) {
		Node res = null;
		while (root != null) {
			if (root.key == key)
				return root;
			else if (root.key > key)
				root = root.left;
			else {
				res = root;
				root = root.right;
			}
		}
		return res;
	}
	
	public Node ceil(Node root, int key) {
		Node res = null;
		while (root != null) {
			if (root.key == key)
				return root;
			else if (root.key < key)
				root = root.right;
			else {
				res = root;
				root = root.left;
			}
		}
		return res;
	}
	
	public void ceilingOnLeftSideInArray(int [] arr) {
		NavigableSet<Integer> set= new TreeSet<>();
		System.out.print("-1 ");
		set.add(arr[0]);
		for(int i=1;i<arr.length;i++) {
			if(set.ceiling(arr[i])!=null)
				System.out.print(set.ceiling(arr[i])+" ");
			else
				System.out.print("-1 ");
			set.add(arr[i]);
		}
		
		
	}

}
