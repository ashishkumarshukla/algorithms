package com.practice.searching;

public class Searching {

	public int binarySearch(int[] arr, int low, int high, int k) {
		if (low > high)
			return -1;
		int mid = low + (high - low) / 2;
		if (arr[mid] == k)
			return mid;
		if (arr[mid] > k)
			return binarySearch(arr, low, mid - 1, k);
		else
			return binarySearch(arr, mid + 1, high, k);

	}

	public int indexOfFirstOccurenceUsingBinarySearch(int[] arr, int low, int high, int k) {
		if (low > high)
			return -1;
		int mid = low + (high - low) / 2;
		if (arr[mid] == k && (mid == 0 || arr[mid - 1] != k))
			return mid;
		if (arr[mid] >= k)
			return indexOfFirstOccurenceUsingBinarySearch(arr, low, mid - 1, k);
		else
			return indexOfFirstOccurenceUsingBinarySearch(arr, mid + 1, high, k);

	}

	public int indexOfLastOccurenceUsingBinarySearch(int[] arr, int low, int high, int k, int n) {
		if (low > high)
			return -1;
		int mid = low + (high - low) / 2;
		if (arr[mid] == k && (mid == n - 1 || arr[mid + 1] != k))
			return mid;
		if (arr[mid] > k)
			return indexOfLastOccurenceUsingBinarySearch(arr, low, mid - 1, k, n);
		else
			return indexOfLastOccurenceUsingBinarySearch(arr, mid + 1, high, k, n);

	}

	public  int squareRoot(int x) {
		int answer = 0;
		if (x == 0 || x == 1)
			return x;
		int start = 1;
		int end = x;
		while (start <= end) {
			int mid = start + (end - start) / 2;
			if (mid * mid == x)
				return mid;
			if (mid * mid < x) {
				start=mid+ 1;
				answer = mid;
			} else
				end = mid - 1;
		}
		return answer;

	}
	
	public boolean isPairWithGivenSumInSortedArrayUsingTwoPointerApproach(int[] arr, int sum) {
		int left = 0;
		int right = arr.length;
		int temp;
		while (left <= right) {
			temp = arr[left] + arr[right];
			if (temp == sum)
				return true;
			else if (temp < sum)
				left++;
			else
				right--;
		}
		return false;
	}
}
