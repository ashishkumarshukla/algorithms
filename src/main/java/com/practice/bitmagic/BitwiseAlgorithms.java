package com.practice.bitmagic;

import java.util.Set;
import java.util.HashSet;

public class BitwiseAlgorithms {

	public boolean isKthBitSet(int n, int k) {

		// left shift
		return ((1 << k) & n) != 0;

		// solution using right shift
		// return ((n>>k)&1) !=0;
	}

	public boolean isNumberPowerOfTwo(int n) {

		if (n == 0 || n == 1)
			return false;
		return ((n & (n - 1)) == 0);
	}

	public int PositionOfMostSignificantSetBit(int n) {

		return (int) (Math.log(n) / Math.log(2));
	}

	public int PositionOfLeastSignificantSetBit(int n) {
		if (n == 0)
			return -1;
		int k = 0;
		while (true) {
			if (((1 << k) & n) != 0)
				return k;
			k++;
		}
	}

	public int positionOfRightMostDiffBit(int m, int n) {
		
		if (m == n)
			return -1;
		int k = 0;
		int mXorN = m ^ n;
		while (true) {
			if (((1 << k) & mXorN) != 0)
				return k;
			k++;
		}
	}

	public int numnerOfSetBitsBrianKerningamAlgo(int n) {
		int count = 0;
		while (n > 0) {
			n = n & (n - 1);
			count++;
		}
		return count;
	}

	public int oneOddOccuringNumber(int arr[]) {

		int num = 0;
		for (int i = 0; i < arr.length; i++)
			num = num ^ arr[i];
		return num;
	}

	public int singleMissingNumberInArrayRange(int arr[], int k) {
		int num = 0;
		for (int i = 0; i < arr.length; i++)
			num = num ^ arr[i];
		for (int j = 1; j <= k; j++)
			num = num ^ j;
		return num;
	}

	public int[] twoOddOccuringNumber(int arr[]) {

		int xor = 0;
		int num1 = 0;
		int num2 = 0;
		for (int i = 0; i < arr.length; i++)
			xor = xor ^ arr[i];
		int rmsBit = xor & ~(xor - 1);
		for (int i = 0; i < arr.length; i++) {
			if ((arr[i] & rmsBit) != 0)
				num1 = num1 ^ arr[i];
			else
				num2 = num2 ^ arr[i];

		}
		return new int[] { num1, num2 };
	}

	public Set<String> printPowerSet(String str) {
		int len = str.length();
		Set<String> powerSet = new HashSet<>();
		powerSet.add("");
		int powSetLen = (int) Math.pow(2, len);
		for (int num = 0; num < powSetLen; num++) {
			String temp = "";
			for (int charIndex = 0; charIndex < len; charIndex++) {
				if (((1 << charIndex) & num) != 0) {
					temp = temp + str.charAt(charIndex);
				}
			}
			powerSet.add(temp);
		}
		return powerSet;
	}

	public int numberOfSetBits(int num) {
		int count = 0;
		for (int i = 1; i <= num; i++) {
			count += numnerOfSetBitsBrianKerningamAlgo(i);
		}

		return count;
	}

	public boolean isSparseInt(int num) {
		return (num & (num >> 1)) == 0;

	}

	public int countBitsFlip(int a, int b) {

		int num = a ^ b;
		int count = 0;
		while (num > 0) {
			num = num & (num - 1);
			count++;
		}
		return count;

	}

	public int maxConsecutiveOnes(int num) {
		int count = 0;
		while (num > 0) {
			num = num & (num << 1);
			count++;
		}
		return count;
	}

	public int greyConverter(int n) {
		return n ^ (n >> 1);
	}

	public int greyToBinaryConverter(int n) {
		return n ^ (n << 1);
		// TODO
	}

	public int swapBits(int n) {

		int evenBits = (n & 0xAAAAAAAA) >> 1;
		int oddBits = (n & 0x55555555) << 1;
		return evenBits | oddBits;
	}

}
