package com.practice.strings;

import java.util.Arrays;

public class Strings {
	
	
	public boolean isAnagramsOfEachOther(String str1, String str2) {
		if(str1.length()!=str2.length())
			return false;
		
		int[]temp=new int[256];
		
		for(int i=0;i<str1.length();i++)
			temp[str1.charAt(i)]+=1;
		for(int i=0;i<str2.length();i++)
			temp[str2.charAt(i)]-=1;
		
		for(int i=0;i<256;i++)
			if(temp[i]!=0)
				return false;
		
		return true;
	}
	
	public int indexOfLeftMostRepeatingChar(String str) {
		int res=Integer.MAX_VALUE;
		int[]temp=new int[256];
		Arrays.fill(temp, -1);
		
		for(int i=0;i<str.length();i++)
			if(temp[str.charAt(i)]==-1)
				temp[str.charAt(i)]=i;
			else
				res=Math.min(temp[str.charAt(i)], res);
		
		return res==Integer.MAX_VALUE?-1:res;
	}
	
	public int indexOfLeftMostNonRepeatingChar(String str) {
		int res=Integer.MAX_VALUE;
		int[]temp=new int[256];
		Arrays.fill(temp, -1);
		
		for(int i=0;i<str.length();i++)
			if(temp[str.charAt(i)]==-1) 
				temp[str.charAt(i)]=i;
			else
				temp[str.charAt(i)]=-2;
		
		for(int i=0;i<256;i++)
			if(temp[i]>=0)
				res=Math.min(temp[i], res);
		
		return res==Integer.MAX_VALUE?-1:res;
	}
	
	public int lexicographicRank(String str) {
		int rank = 1;
		int len = str.length();
		int mul = factorial(len, 1);
		int[] count = new int[256];
		for (int i = 0; i < len; i++)
			count[str.charAt(i)] += 1;
		for (int i = 1; i < 256; i++)
			count[i] += count[i - 1];
		for (int i = 0; i < len; i++) {
			mul = mul / (len - i);
			rank += count[str.charAt(i) - 1] * mul;
			for (int j = str.charAt(i); j < 256; j++)
				count[j]--;
		}

		return rank;
	}
	
	private int factorial(int n, int k) {
		if (n == 0)
			return 1 * k;
		return factorial(n - 1, n * k);
	}
	
	public boolean angramPatternSearch(String str,String pattern) {
		if(str.length()<pattern.length())
			return false;
		
		int[] tempStr = new int[256];
		int[] tempPattern = new int[256];
			
		for (int i = 0; i < pattern.length(); i++) {
			tempStr[str.charAt(i)] += 1;
			tempPattern[pattern.charAt(i)] += 1;
		}
		
		
		for(int i=pattern.length()-1;i<str.length();i++) {
			if(areSame(tempStr,tempPattern))
				return true;
			else {
				tempStr[str.charAt(i)]+=1;
				tempStr[str.charAt(i-pattern.length()+1)]-=1;
				
			}
		}
		
		return false;
	}


	private boolean areSame(int[] tempStr, int[] tempPattern) {
       
		for(int i=0;i<256;i++)
			if(tempStr[i]!=tempPattern[i])
				return false;
		return true;
	}
	
    public boolean areRotations(String strOne, String strTwo) {
    	if(strOne.length()!=strTwo.length())
    		return false;
    	strOne+=strOne;
    	return strOne.indexOf(strTwo)>=0;
    }

}
