package com.practice.linkedlist;

public class List<T> {

	Node head;
	class Node {
		T data;
		Node next;
		Node(T d) {
			data = d;
		}
	}

	public void insertAtSatrt(T x) {
		Node temp = new Node(x);
		temp.next = head;
		head = temp;

	}

	public void insertAtEnd(T x) {
		Node temp = new Node(x);
		if (head == null) {
			head = temp;
			return;
		}

		Node curr = head;
		while (curr.next != null)
			curr = curr.next;
		curr.next = temp;

	}
	public void printList() {
		Node curr = head;
		while (curr != null) {
			System.out.println(curr.data.toString());
			curr = curr.next;
		}
	}
	
	public void printMiddleOfList() {
		if (head == null)
			return ;
		Node slow = head;
		Node fast = head;
		while (fast!=null &&fast.next != null) {
			slow=slow.next;
			fast=fast.next.next;
		}
		System.out.println(slow.data.toString());
	
	}
	
	public T nthElementFromEnd(int n) {
		if (head == null || n<=0)
			return null;
		Node slow = head;
		Node fast = head;
		for(int i=0;i<n;i++) {
			if(fast==null)
				return null;
			fast=fast.next;
		}
		
		while(fast!=null) {
			fast=fast.next;
			slow=slow.next;
		}
		return slow.data;
		
	}

	public void reverse() {
		 Node prev=null;
		 Node curr=head;
		 while(curr!=null) {
			 head=curr;
			 Node next=curr.next;
             curr.next=prev;
             prev=curr;
             curr=next;
		 }
	}
	
	public void reverseRecursively() {
		recursiveReverse(head);
		
	}
	
	private void recursiveReverse(Node head) {
		if(head==null || head.next==null)
			return ;
		this.head=head.next;
		recursiveReverse(head.next);
		head.next.next=head;
		head.next=null;
		
	}
	

}