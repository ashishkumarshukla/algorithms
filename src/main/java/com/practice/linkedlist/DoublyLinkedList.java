package com.practice.linkedlist;

public class DoublyLinkedList<T> {

	Node head;
	class Node {
		T data;
		Node next;
		Node prev;
		Node(T d) {
			data = d;
		}
	}

	public void insertAtSatrt(T x) {
		Node temp = new Node(x);
		temp.next = head;
		temp.prev = null;
		if (head != null)
			head.prev = temp;
		head = temp;

	}

	public void insertAtEnd(T x) {
		Node temp = new Node(x);
		if (head == null) {
			head = temp;
			return;
		}

		Node curr = head;
		while (curr.next != null)
			curr = curr.next;
		curr.next = temp;
		temp.prev = curr;
	}

}
