package com.practice.recursion;

public class Recursion {

	public int sumOfDigits(int n) {
		return tailRecursiveSumOfDigits(n, 0);
	}

	private int tailRecursiveSumOfDigits(int n, int k) {
		if (n < 10)
			return n + k;
		return tailRecursiveSumOfDigits(n / 10, k + n % 10);

	}

	public int countDigits(int n) {
		return tailRecursiveCountDigits(n, 0);
	}

	private int tailRecursiveCountDigits(int n, int k) {
		if (n == 0)
			return k;
		return tailRecursiveCountDigits(n / 10, k + 1);
	}

	public void printNumberFromNToOne(int n) {
		if (n == 0)
			return;
		System.out.print(n + " ");
		printNumberFromNToOne(n - 1);
	}

	public void printNumberFromOneToN(int n) {
		tailRecursivePrintNumberFromOneToN(n, 1);

	}

	private void tailRecursivePrintNumberFromOneToN(int n, int k) {
		if (n == 0)
			return;
		System.out.print(k + " ");
		tailRecursivePrintNumberFromOneToN(n - 1, k + 1);

	}

	public int factorial(int n) {

		return tailRecursiveFactorial(n, 1);
	}

	private int tailRecursiveFactorial(int n, int k) {
		if (n == 0)
			return 1 * k;
		return tailRecursiveFactorial(n - 1, n * k);
	}

	public boolean isPalindrome(String str) {
		if (null == str)
			return false;
		return isPalindrome(str, 0, str.length() - 1);
	}

	private boolean isPalindrome(String str, int start, int end) {
		if (start == end)
			return true;
		if (start > end)
			return true;
		if (str.charAt(start) != str.charAt(end))
			return false;

		return isPalindrome(str, start + 1, end - 1);
	}

	public int nthFibonacciNumber(int n) {
		return tailRecursiveNthFibonacciNumber(n, 0, 1);
	}

	private int tailRecursiveNthFibonacciNumber(int n, int a, int b) {
		if (n == 0)
			return a;
		if (n == 1)
			return b;
		return tailRecursiveNthFibonacciNumber(n - 1, b, a + b);
	}

	public int maxCutsWithGivenLengthRange(int length, int a, int b, int c) {

		int result;
		if (length == 0)
			return 0;
		if (length < 0)
			return -1;

		result = max(maxCutsWithGivenLengthRange(length - a, a, b, c), maxCutsWithGivenLengthRange(length - b, a, b, c),
				maxCutsWithGivenLengthRange(length - c, a, b, c));
		if (result == -1)
			return -1;

		return result + 1;
	}

	private int max(int a, int b, int c) {
		return Math.max(a, Math.max(b, c));
	}

	public int digitalRoot(int n) {
		return tailRecursivedigitalRoot(n, 0);
	}

	private int tailRecursivedigitalRoot(int n, int k) {
		int res;
		if (n < 10)
			return n + k;
		res = tailRecursivedigitalRoot(n / 10, k + n % 10);
		if (res >= 10)
			res = tailRecursivedigitalRoot(res, 0);
		return res;
	}

	public long power(int N, int R) {
		long res;
		if (R == 1)
			return N;
		else
			res = N * power(N, R - 1);
		return res % 1000000007;
	}

	public void towerOfHanoi(int n, String source, String dest, String temp) {
		if (n == 1) {
			System.out.println("move disc " + n + " from " + source + " to " + dest);
		} else {
			towerOfHanoi(n - 1, source, temp, dest);
			System.out.println("move disc " + n + " from " + source + " to " + dest);
			towerOfHanoi(n - 1, temp, dest, source);
		}
	}

	public  int josephus(int n, int k) {
        if(n==1)
        	return 0;
		return (josephus(n-1,k)+k)%n;
	}
}